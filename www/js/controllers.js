angular.module('starter.controllers',[])

.run(function(translator,Users,$rootScope){
  $rootScope.lang = "kr";
  $rootScope.translator = translator.getTranslator();

})

.directive('ngEnter', function () {
    return function (scope, elements, attrs) {
        elements.bind('keydown keypress', function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter);
                });
                event.preventDefault();
            }
        });
    };
})

.controller('AccountCtrl',function($scope,$rootScope,$ionicPopup,Users,Tutors,Students){
  $scope.t = $scope.translator.accountPage;

  $scope.login = function(id, password){
    var user = Users.login(id,password);
    if(!user) {
      var alertPopup = $ionicPopup.alert({
        title: '로그인  실패',
        template: '아이디와 비밀번호를 다시 확인하십시오.'
      });
      return;
    }
    if(user.role == "tutor"){
      user.profile = Tutors.getTutorById(user.profile_id);
    } else {
      user.profile = Students.getStudentById(user.profile_id);
    }
    $rootScope.user = user;
  }
  $scope.logout = function(){
    $rootScope.user = undefined;
  }

})

.controller('AccountDefaultCtrl',function($scope){
  $scope.t = $scope.translator.accountPage;
})

.controller('AccountProfileCtrl',function($scope){
  var _profile = angular.copy($scope.user.profile);

  if($scope.user.role == "tutor"){
    delete _profile.id;
    delete _profile.user_id;
    delete _profile.user;
  } else {
    delete _profile.id;
    delete _profile.user_id;
    delete _profile.user;
  }

  $scope.data = {
    profile : _profile
  };
})

.controller('AccountLanguageCtrl',function($scope,$rootScope,translator){
  $scope.languages = translator.getLanguages();
  $scope.data = {
    lang : $rootScope.lang
  };
  $scope.changeLang = function(item){
    //console.log("lang, text:",item.text,"value",item.value);
    $rootScope.lang = $scope.data.lang;
  }
})

.controller('AccountBookmarksCtrl',function($scope,translator,Users,Tutors,Students){
  var wish_students = $scope.user.profile.wish_students;
  var wish_tutors = $scope.user.profile.wish_teachers;
  var students=[];
  var tutors=[];

  if(wish_students){
    //console.log(wish_students)
    for(var i in wish_students){
      var student = Students.getStudentById(wish_students[i].student_id);
      student.user = Users.getUserById(student.user_id);
      students.push(student);
    }

    $scope.students = students;
  } else {
    //console.log(wish_teachers)
    for(var i in wish_tutors){
      var tutor = Tutors.getTutorById(wish_tutors[i].tutor_id);
      tutor.user = Users.getUserById(tutor.user_id);
      tutors.push(tutor);
    }

    $scope.tutors = tutors;
  }


  /*
  if($scope.user.role=='student'){
    $scope.t = translator.tutorsPage;
    var tutors = Tutors.getTutors();
    for(var id in tutors){
      tutors[id].user = Users.getUserById(tutors[id].user_id);
    }
    $scope.tutors = tutors;
  } else {
    $scope.t = translator.studentsPage;
    var students = Students.getStudents();
    for(var id in students){
      students[id].user = Users.getUserById(students[id].user_id)
    }
    $scope.students = students;
  }

  $scope.getConditionString = function(obj){
    var s = "";
    for(var id in obj){
      if(obj[id]) s += $scope.t[id][$scope.lang]+" ";
    }
    return s;
  }
  $scope.getAddrString = function(tutor){
    var value = "";
    for(var id in tutor.addr){
      if(tutor.addr[id]){
        value += $scope.t[id][$scope.lang]+" ";
      }
    }
    return value;
  }
  $scope.getScheduleString = function(tutor){
    var value="";
    for(var id in tutor.avail_schedule){
      if(tutor.avail_schedule[id]){
        value += $scope.t[id][$scope.lang]+" ";
      }
    }
    return value;
  }
  */
})

.controller('TutorsCtrl', function($scope,Tutors,Users){
  $scope.t = $scope.translator.tutorsPage;
  var tutors = Tutors.getTutors();

  for(var id in tutors){
    tutors[id].user = Users.getUserById(tutors[id].user_id);
  }

  $scope.tutors = tutors;

  $scope.getAddrString = function(tutor){
    var value = "";
    for(var id in tutor.addr){
      if(tutor.addr[id]){
        value += $scope.t[id][$scope.lang]+" ";
      }
    }
    return value;
  }
  $scope.getScheduleString = function(tutor){
    var value="";
    for(var id in tutor.avail_schedule){
      if(tutor.avail_schedule[id]){
        value += $scope.t[id][$scope.lang]+" ";
      }
    }
    return value;
  }
})

.controller('TutorDetailCtrl',function($scope,$stateParams,$ionicPopup,$ionicScrollDelegate,Tutors,Users,$timeout){
  //tutor init
  var tutor = Tutors.getTutorById($stateParams.tutorId);
  tutor.user = Users.getUserById(tutor.user_id);
  $scope.tutor = tutor;

  //translator
  $scope.t = $scope.translator.tutorsPage;
  $scope.getConditionString = function(obj){
    var s = "";
    for(var id in obj){
      if(obj[id]) s += $scope.t[id][$scope.lang]+" ";
    }
    return s;
  }

  $scope.getSpecString = function(obj){
    var s = "";
    for(var id in obj){
      if(obj[id]) s += $scope.t[id][$scope.lang]+"["+obj[id]+"] ";
    }
    return s;
  }

  //boomarks
  $scope.addBookmark = function(id){
    var bookmarks = $scope.user.profile.wish_teachers;

    var confirmPopup = function(){
      var confirmPopup = $ionicPopup.confirm({
        title:'튜터 북마크 등록',
        template:'이 튜터를 북마크에 등록하시겠습니까? 목록은 Account-튜터북마크목록 에서 확인 가능합니다.'
      }).then(function(res){
        if(res){
          bookmarks.unshift({"tutor_id":id,"created":new Date()});
        }
        //console.log(bookmarks);
      });
    }
    var alertPopup = function(){
      var alertPopup = $ionicPopup.alert({
        title: '이미 북마크된 튜터입니다.',
        template: '이미 북마크된 튜터입니다.'
      });
    }

    var exist = false;
    for(var i in bookmarks){
      if(bookmarks[i].tutor_id == id) exist = true;
    }

    if(!exist) confirmPopup();
    else alertPopup();
  }

  //comments
  var comments = $scope.tutor.comments;
  $scope.commentBtn = false;
  $scope.pushCommentBtn = function(){
    $scope.commentBtn = ! $scope.commentBtn;
    $timeout(function(){
      $ionicScrollDelegate.resize();
    },50);
  }
  $scope.$watch("tutor.comments.length",function(){
    //console.log("watch")
    for(var i in comments){
      var currentUser = Users.getUserById(comments[i].user_id);
      comments[i].name = currentUser.name;
      comments[i].img = currentUser.img;
      comments[i].role = currentUser.role;
    }
    $timeout(function(){
      $ionicScrollDelegate.resize();
    },50);
  })
  $scope.postComment = function(text){
    var commentObj = {};
    commentObj.comment = text;
    commentObj.user_id = $scope.user.id;
    commentObj.created = new Date();
    tutor.comments.unshift(commentObj);
    //console.log(tutor.comments);
  }

  //recommended
  $scope.recommend = function(){

    for(var i in tutor.recommended){
       if ($scope.user.id == tutor.recommended[i].user_id){
         var alertPopup = $ionicPopup.alert({
           title: '추천',
           template: '이미 추천한 튜터입니다.'
         })
         return;
       }
    }

    var recommendData={
      user_id:$scope.user.id,
      active:true
    };
    tutor.recommended.unshift(recommendData);
    //console.log(tutor.recommended);
  }
})

.controller('StudentsCtrl', function($scope,Students,Users){
  $scope.t = $scope.translator.studentsPage;
  var students = Students.getStudents();
  for(var id in students){
    students[id].user = Users.getUserById(students[id].user_id)
  }
  $scope.students = students;

  $scope.getConditionString = function(obj){
    var s = "";
    for(var id in obj){
      if(obj[id]) s += $scope.t[id][$scope.lang]+" ";
    }
    return s;
  }
})

.controller('StudentDetailCtrl',function($scope,$stateParams,$ionicPopup,Students,Users){
  $scope.t = $scope.translator.studentsPage;
  var student = Students.getStudentById($stateParams.studentId);
  student.user = Users.getUserById(student.user_id);
  $scope.student = student;

  $scope.getConditionString = function(obj){
    var s = "";
    for(var id in obj){
      if(obj[id]) s += $scope.t[id][$scope.lang]+" ";
    }
    return s;
  }

  $scope.addBookmark = function(id){
    var bookmarks = $scope.user.profile.wish_students;

    var confirmPopup = function(){
      var confirmPopup = $ionicPopup.confirm({
        title:'학생 북마크 등록',
        template:'이 학생을 북마크에 등록하시겠습니까? 목록은 Account-학생찜목록 에서 확인 가능합니다.'
      }).then(function(res){
        if(res){
          bookmarks.unshift({"student_id":id,"created":new Date()});
        }
        //console.log($scope.user.profile.wish_students);
      });
    }
    var alertPopup = function(){
      var alertPopup = $ionicPopup.alert({
        title: '이미 북마크된 학생입니다.',
        template: '이미 북마크된 학생입니다.'
      });
    }

    var exist = false;
    for(var i in bookmarks){
      if(bookmarks[i].student_id == id) exist = true;
    }

    if(!exist) confirmPopup();
    else alertPopup();
  }

})
