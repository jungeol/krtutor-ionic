// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider){
  $stateProvider
  .state('tab', {
    url:'/tab',
    abstract:true,
    templateUrl:'templates/tabs.html'
  })
  // Each tab has its own nav history stack:

  .state('tab.account', {
    url: '/account',
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  })

  .state('tab.default',{
    url:'/default',
    views:{
      'tab-account':{
        templateUrl: 'templates/account-default.html',
        controller: 'AccountDefaultCtrl'
      }
    }
  })

  .state('tab.profile',{
    url:'/profile',
    views:{
      'tab-account':{
        templateUrl: 'templates/account-profile.html',
        controller: 'AccountProfileCtrl'
      }
    }
  })

  .state('tab.language',{
    url: '/language',
    views:{
      'tab-account':{
        templateUrl: 'templates/account-language.html',
        controller: 'AccountLanguageCtrl'
      }
    }
  })

  .state('tab.bookmarks',{
    url: '/bookmarks',
    views:{
      'tab-account':{
        templateUrl: 'templates/account-bookmarks.html',
        controller: 'AccountBookmarksCtrl'
      }
    }
  })

  .state('tab.bookmarks-tutor',{
    url: '/booktutor/:tutorId',
    views:{
      'tab-account':{
        templateUrl: 'templates/tutor-detail.html',
        controller: 'TutorDetailCtrl'
      }
    }
  })

  .state('tab.bookmarks-student',{
    url: '/bookstudent/:studentId',
    views:{
      'tab-account':{
        templateUrl: 'templates/student-detail.html',
        controller: 'StudentDetailCtrl'
      }
    }
  })

  .state('tab.tutors', {
      url: '/tutors',
      views: {
        'tab-tutors': {
          templateUrl: 'templates/tab-tutors.html',
          controller: 'TutorsCtrl'
        }
      }
    })

  .state('tab.tutor-detail',{
    url: '/tutors/:tutorId',
    views:{
      'tab-tutors':{
        templateUrl: 'templates/tutor-detail.html',
        controller: 'TutorDetailCtrl'
      }
    }
  })

  .state('tab.students', {
    url: '/students',
    views: {
      'tab-students': {
        templateUrl: 'templates/tab-students.html',
        controller: 'StudentsCtrl'
      }
    }
  })

  .state('tab.student-detail',{
    url: '/students/:studentId',
    views:{
      'tab-students':{
        templateUrl: 'templates/student-detail.html',
        controller: 'StudentDetailCtrl'
      }
    }
  })

  $urlRouterProvider.otherwise('/tab/account');
});
