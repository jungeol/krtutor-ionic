angular.module('starter.services',[])

.factory('translator', function(){
  var languages = [
    {text:"English",value:"en"},
    {text:"Korean",value:"kr"}
  ];

  var translator = {
    tutorsPage:{
      tutors:{en:"Tutors",kr:"튜터 리스트"},
      tutorDetail:{en:"Tutor-Detail",kr:"튜터 상세"},
      //thumbnail
      age:{en:"Age",kr:"나이"},
      gender:{en:"Gender",kr:"성별"},
      recommended:{en:"Recommended",kr:"추천"},
      comments:{en:"Comments",kr:"댓글"},
      //introduction
      introduction:{en:"Introduction",kr:"소개"},
      //qualification
      qualification:{en:"Qualification",kr:"스팩"},
        level:{en:"Level",kr:"레벨"},
          levelText:[
           {en:"Korean Native Speaker",kr:"한국어 네이티브 스피커"},
           {en:"Korean Teacher License Level 1",kr:"한국어 교원자격증 3급"},
           {en:"Korean Teacher License Level 2",kr:"한국어 교원자격증 2급"},
           {en:"Korean Teacher License Level 3",kr:"한국어 교원자격증 1급"}],
         education:{en:"Education",kr:"학력"},
           bacahelor:{en:"Bacahelor",kr:"학사"},
           master:{en:"Master",kr:"석사"},
           doctor:{en:"Doctor",kr:"박사"},
         experience:{en:"Experience",kr:"경력"},
           //subjects하위와 중복
      //conditions
      conditions:{en:"Conditions",kr:"조건"},
        avail_address:{en:"Avail_address",kr:"가능지역"},
          seogu:{en:"Sougu",kr:"서구"},
          yuseongu:{en:"Yuseongu",kr:"유성구"},
          jungu:{en:"Jungu",kr:"중구"},
          dongu:{en:"Dongu",kr:"동구"},
          daedukgu:{en:"Daedukgu",kr:"대덕구"},
        subjects:{en:"Subjects",kr:"가능과목"},
          speaking:{en:"Speaking",kr:"스피킹"},
          exam:{en:"Exam",kr:"시험"},
          grammer:{en:"Grammer",kr:"문법"},
        avail_schedule:{en:"Avail_schedule", kr:"가능요일"},
          mon:{en:"Mon",kr:"월"},
          tue:{en:"Tue",kr:"화"},
          wed:{en:"Wed",kr:"수"},
          thur:{en:"Thur",kr:"목"},
          fri:{en:"Fri",kr:"금"},
          sat:{en:"Sat",kr:"토"},
          sun:{en:"Sun",kr:"일"},
        fee:{en:"Fee",kr:"희망금액"},
          currency:{en:"0,000 Won",kr:"만원"},
      //contacts
      contacts:{en:"Contacts",kr:"연락처"},
        phone:{en:"Phone",kr:"핸드폰"},
        kakaotalk:{en:"Kakaotalk",kr:"카카오톡"},
        notStudent:{en:"No Student, No Contacts",kr:"학생 아이디로 로그인해야 볼 수 있습니다."},
        notStudentLink:{en:"Login",kr:"로그인"},
    },
    studentsPage:{
      /*list*/
      students:{en:"Students",kr:"학생 리스트"},
      langLevel:{en:"Language Level",kr:"한국어 실력"},
        langLevelText:[
          {en:"Beginner",kr:"초급"},
          {en:"Upper Beginner",kr:"초중급"},
          {en:"Intermediate",kr:"중급"},
          {en:"Upper Intermediate",kr:"중고급"},
          {en:"Advanced",kr:"고급"}],
      //wishSchedule:{en:"Wish Schedule",kr:"희망요일"},

      avail_schedule:{en:"Avail_schedule", kr:"가능요일"},
        mon:{en:"Mon",kr:"월"},
        tue:{en:"Tue",kr:"화"},
        wed:{en:"Wed",kr:"수"},
        thur:{en:"Thur",kr:"목"},
        fri:{en:"Fri",kr:"금"},
        sat:{en:"Sat",kr:"토"},
        sun:{en:"Sun",kr:"일"},
        preTimesPerWeek:{en:"",kr:"주 "},
        postTimesPerWeek:{en:"times a week",kr:"회"},
      address:{en:"Address",kr:"지역"},
        seogu:{en:"Sougu",kr:"서구"},
        yuseongu:{en:"Yuseongu",kr:"유성구"},
        jungu:{en:"Jungu",kr:"중구"},
        dongu:{en:"Dongu",kr:"동구"},
        daedukgu:{en:"Daedukgu",kr:"대덕구"},
      fee:{en:"Fee",kr:"희망금액"},
        currency:{en:"0,000 Won",kr:"만원"},
      /*detail*/
      studentDetail:{en:"Student-Detail",kr:"학생 상세"},
      //thumbnail
      age:{en:"Age",kr:"나이"},
      gender:{en:"Gender",kr:"성별"},
      recommended:{en:"Recommended",kr:"추천"},
      comments:{en:"Comments",kr:"댓글"},
      //introduction
      introduction:{en:"Introduction",kr:"소개"},
      //information
      information:{en:"Information",kr:"정보"},
        //lang_level:already exist
        //addr:already exist
        isGroup:{en:"Group Tutoring",kr:"그룹과외"},
          isGroupText:[{en:"Yes",kr:"가능"},{en:"No",kr:"불가능"}],
      //conditions
      conditions:{en:"Conditions",kr:"조건"},
        //avail_schedule:already exist
          //timesPerWeek:already exist
        subjects:{en:"Subjects",kr:"가능과목"},
          speaking:{en:"Speaking",kr:"스피킹"},
          exam:{en:"Exam",kr:"시험"},
          grammer:{en:"Grammer",kr:"문법"},
      //contacts
      contacts:{en:"Contacts",kr:"연락처"},
        phone:{en:"Phone",kr:"핸드폰"},
        kakaotalk:{en:"Kakaotalk",kr:"카카오톡"},
        notTutor:{en:"No Tutor, No Contacts",kr:"튜터 아이디로 로그인해야 볼 수 있습니다."},
        notTutorLink:{en:"Login",kr:"로그인"}
    },
    accountPage:{
      account:{en:"Account",kr:"회원정보"},
      language:{en:"Language",kr:"언어(Language)"},
      defaultInfo:{en:"My Default Information",kr:"내 기본 정보"},
      tutorInfo:{en:"My Tutor Information",kr:"내 튜터 정보"},
      studentInfo:{en:"My Student Information",kr:"내 학생 정보"},
      logout:{en:"Log out",kr:"로그아웃"},
      bookmarkStudents:{en:"My Students Bookmark",kr:"학생 북마크"},
      bookmarkTutors:{en:"My Tutors Bookmark",kr:"튜터 북마크"},
      //default page

    }
  };

  return {
    getTranslator : function(){ return translator; },
    getLanguages : function(){ return languages }
  };
})

.factory('Users', function(){
  var users = {
    //tutors
    id0:{
      id:"id0",
      profile_id:"id0",
      username:"tutor",
      password:"tutor",
      name:"Jungeol",
      age:26,
      gender:"M",
      img:"img/pic1.jpg",
      role:"tutor",
      last_login:"",
      contacts:{phone:"010-0000-0000", kakaotalk:"blabla"}
    },
    id1:{
      id:"id1",
      profile_id:"id1",
      username:"email@email.com",
      password:"pass",
      name:"Enseok",
      age:28,
      gender:"M",
      img:"img/pic2.jpg",
      role:"tutor",
      last_login:"",
      contacts:{phone:"010-0000-0000", kakaotalk:"blabla"}
    },
    id2:{
      id:"id2",
      profile_id:"id2",
      username:"email@email.com",
      password:"pass",
      name:"Gildong",
      age:30,
      gender:"M",
      img:"img/pic3.png",
      role:"tutor",
      last_login:"",
      contacts:{phone:"010-0000-0000", kakaotalk:"blabla"}
    },
    id3:{
      id:"id3",
      profile_id:"id3",
      username:"email@email.com",
      password:"pass",
      name:"DaHyeok",
      age:41,
      gender:"M",
      img:"img/pic4.png",
      role:"tutor",
      last_login:"",
      contacts:{phone:"010-0000-0000", kakaotalk:"blabla"}
    },
    id4:{
      id:"id4",
      profile_id:"id4",
      username:"email@email.com",
      password:"pass",
      name:"Jintae",
      age:26,
      gender:"M",
      img:"img/pic5.png",
      role:"tutor",
      last_login:"",
      contacts:{phone:"010-0000-0000", kakaotalk:"blabla"}
    },
    //students
    id5:{
      id:"id5",
      profile_id:"id5",
      role:"student",
      username:"student",
      password:"student",
      name:"Mike",
      age:20,
      gender:"M",
      img:"img/pic1.jpg",
      last_login:"",
      contacts:{phone:"010-0000-0000",kakaotalk:"blabla"}
    },
    id6:{
      id:"id6",
      profile_id:"id6",
      role:"student",
      username:"id6",
      password:"id6",
      name:"Kebin",
      age:30,
      gender:"M",
      img:"img/pic2.jpg",
      last_login:"",
      contacts:{phone:"010-0000-0000",kakaotalk:"blabla"}
    },
    id7:{
      id:"id7",
      profile_id:"id7",
      role:"student",
      username:"id7",
      password:"id7",
      name:"Sam",
      age:25,
      gender:"M",
      img:"img/pic3.png",
      last_login:"",
      contacts:{phone:"010-0000-0000",kakaotalk:"blabla"}
    },
    id8:{
      id:"id8",
      profile_id:"id8",
      role:"student",
      username:"id8",
      password:"id8",
      name:"Jun",
      age:28,
      gender:"M",
      img:"img/pic4.png",
      last_login:"",
      contacts:{phone:"010-0000-0000",kakaotalk:"blabla"}
    },
    id9:{
      id:"id9",
      profile_id:"id9",
      role:"student",
      username:"id9",
      password:"id9",
      name:"Adolf",
      age:31,
      gender:"M",
      img:"img/pic5.png",
      last_login:"",
      contacts:{phone:"010-0000-0000",kakaotalk:"blabla"}
    }
  };

  return {
    getUsers : function(){ return users; },
    getUserById : function(id){ return users[id] },
    login: function(username,password){
      var user;
      for(var id in users){
        if(users[id].username==username && users[id].password==password){
          user = users[id];
          break;
        }
      }
      //console.log(user);
      return user;
    }
  };
})

.factory('Tutors',function(){
  var tutors = {
    id0:{
      id:"id0",
      user_id:"id0",
      title:'최고의 튜터, 자신있습니다!',
      cert_level:3,
      recommended:[],
      addr: {seogu:true,yuseongu:false,jungu:true,dongu:true,daedukgu:false },
      subjects: {speaking:false,exam:false,grammer:true},
      fee:[15,20],
      avail_schedule:{mon:false,tue:true,wed:false,thur:true,fri:false,sat:false,sun:false},
      education:{ bacahelor: "서울대(국어국문)", master: "", doctor: "" },
      experience:{ speaking:[3,3], exam:[1,1], grammer:[2,3] },
      introduction:"최고의 튜터가 될 자신있습니다!",
      is_group:true,
      wish_students:[],
      comments:[{user_id:"id1",comment:"Good!"},{user_id:"id5",comment:"Perfect!"}]
    },
    id1:{
      id:"id1",
      user_id:"id1",
      title:'확실한 교육!',
      cert_level:2,
      recommended:[],
      addr: {seogu:false,yuseongu:true,jungu:false,dongu:false,daedukgu:true },
      subjects: {speaking:true,exam:true,grammer:true},
      fee:[15,15],
      avail_schedule:{mon:true,tue:true,wed:true,thur:true,fri:true,sat:false,sun:false},
      education:{ bacahelor: "", master: "연세대(경영)", doctor: "" },
      experience:{ speaking:[3,3], exam:[1,1], grammer:[2,3] },
      introduction:"확실하게 가르칠수있습니다.",
      is_group:false,
      wish_students:[],
      comments:[]
    },
    id2:{
      id:"id2",
      user_id:"id2",
      title:'자상하게 가르쳐드립니다.',
      cert_level:1,
      recommended:[],
      addr: {seogu:false,yuseongu:false,jungu:false,dongu:false,daedukgu:true },
      subjects: {speaking:true,exam:false,grammer:false},
      fee:[10,15],
      avail_schedule:{mon:false,tue:true,wed:false,thur:true,fri:false,sat:false,sun:false},
      education:{ bacahelor: "고려대", master: "", doctor: "" },
      experience:{ speaking:[3,3], exam:[1,1], grammer:[2,3] },
      introduction:"자상하게 가르쳐드립니다.",
      is_group:false,
      wish_students:[],
      comments:[]
    },
    id3:{
      id:"id3",
      user_id:"id3",
      title:'교육학 석사가 가르치는 전문 과외!',
      cert_level:3,
      recommended:[],
      addr: {seogu:false,yuseongu:true,jungu:false,dongu:false,daedukgu:false },
      subjects: {speaking:false,exam:false,grammer:true},
      fee:[25,30],
      avail_schedule:{mon:false,tue:false,wed:false,thur:true,fri:false,sat:true,sun:true},
      education:{ bacahelor: "", master: "동국대(교육)", doctor: "" },
      experience:{ speaking:[3,3], exam:[1,1], grammer:[2,3] },
      introduction:"교육학 석사가 가르치는 전문 과외!",
      is_group:false,
      wish_students:[],
      comments:[]
    },
    id4:{
      id:"id4",
      user_id:"id4",
      title:'원하는시간에 원하는 만큼 가르쳐드립니다.',
      cert_level:4,
      recommended:[],
      addr: {seogu:true,yuseongu:false,jungu:true,dongu:true,daedukgu:false },
      subjects: {speaking:false,exam:false,grammer:true},
      fee:[20,25],
      avail_schedule:{mon:false,tue:true,wed:false,thur:true,fri:false,sat:false,sun:false},
      education:{ bacahelor: "", master: "서울시립대", doctor: "" },
      experience:{ speaking:[3,3], exam:[1,1], grammer:[2,3] },
      introduction:"원하는시간에 원하는 만큼 가르쳐드립니다.",
      is_group:true,
      wish_students:[],
      comments:[]
    }
  };

  return {
    getTutors : function(){return tutors;},
    getTutorById : function(id){return tutors[id];}
  };
})

.factory('Students',function(){
  var students = {
    id5:{
      id:"id5",
      user_id:"id5",
      title:"한국어 초급인데 봐주실분 구합니다.",
      lang_level:1,
      avail_schedule:{mon:false,tue:true,wed:false,thur:true,fri:false,sat:false,sun:false},
      avail_tutoring_num:3,
      addr:{seogu: true,yuseongu: false,jungu: false,dongu: false,daedukgu: false},
      subjects:{speaking:false,exam:false,grammer:true},
      id_group:false,
      introduction:"한국어 초급인데 봐주실분 구합니다. blabla",
      wish_teachers:[],
      fee:[15,20]
    },
    id6:{
      id:"id6",
      user_id:"id6",
      title:"한국 사는거 도움 주실분~",
      lang_level:2,
      avail_schedule:{mon:false,tue:false,wed:false,thur:false,fri:false,sat:true,sun:true},
      avail_tutoring_num:2,
      addr:{seogu: false,yuseongu: true,jungu: false,dongu: false,daedukgu: false},
      subjects:{speaking:true,exam:false,grammer:true},
      id_group:false,
      introduction:"한국 사는거 도움 주실분~ blabla",
      wish_teachers:[],
      fee:[10,20]
    },
    id7:{
      id:"id7",
      user_id:"id7",
      title:"한국어 시험 준비중입니다.",
      lang_level:3,
      avail_schedule:{mon:false,tue:true,wed:true,thur:true,fri:false,sat:false,sun:false},
      avail_tutoring_num:3,
      addr:{seogu: false,yuseongu: false,jungu: true,dongu: false,daedukgu: false},
      subjects:{speaking:false,exam:true,grammer:true},
      id_group:false,
      introduction:"한국어 시험 준비중입니다. 진지하게 도와주실분. blabla",
      wish_teachers:[],
      fee:[20,25]
    },
    id8:{
      id:"id8",
      user_id:"id8",
      title:"한국어 너무 어려워요",
      lang_level:1,
      avail_schedule:{mon:false,tue:true,wed:false,thur:true,fri:false,sat:false,sun:false},
      avail_tutoring_num:4,
      addr:{seogu: false,yuseongu: false,jungu: false,dongu: true,daedukgu: false},
      subjects:{speaking:true,exam:false,grammer:false},
      id_group:true,
      introduction:"한국어 너무 어려워요. blabla",
      wish_teachers:[],
      fee:[18,20]
    },
    id9:{
      id:"id9",
      user_id:"id9",
      title:"수준높은 튜터 원합니다.",
      lang_level:5,
      avail_schedule:{mon:false,tue:true,wed:false,thur:true,fri:false,sat:false,sun:false},
      avail_tutoring_num:2,
      addr:{seogu: false,yuseongu: false,jungu: false,dongu: false,daedukgu: true},
      subjects:{speaking:true,exam:false,grammer:true},
      id_group:false,
      introduction:"수준높은 튜터 원합니다. blabla",
      wish_teachers:[],
      fee:[20,30]
    },
  }

  return {
    getStudents:function(){return students},
    getStudentById:function(id){return students[id]}
  }
})
